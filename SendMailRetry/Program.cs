﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;

namespace SendMailRetry
{
	class Program
	{
		public static class SysLog
		{
			public static void LogException(Exception ex, string appRelativeVirtualPath)
			{
				EventLog.WriteEntry("ASP.NET 2.0.50727.0", $"**** GC Custom Logged Exception (Unexpected) ****\r\n\r\n    SendMailRetryIteration: {appRelativeVirtualPath}\r\n    DateTime: {DateTime.Now.ToString()}\r\n    StackTrace: {ex.ToString()}", EventLogEntryType.Warning, 100, 3);
			}
		}

		public static class DB
		{
			public static DataSet GetDS(string Sql, SqlConnection dbconn)
			{
				DataSet dataSet = new DataSet();
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(Sql, dbconn);
				sqlDataAdapter.Fill(dataSet, "Table");
				return dataSet;
			}

			public static string GetSqlS(string Sql, SqlConnection dbconn)
			{
				string result = string.Empty;
				using (IDataReader dataReader = GetRS(Sql, dbconn))
				{
					if (dataReader.Read())
					{
						result = AppLogic.SafeStr(dataReader["S"]);
					}
				}
				return result;
			}

			public static List<string> GetDBConnList()
			{
				List<string> list = new List<string>();
				foreach (string key in ConfigurationSettings.AppSettings.Keys)
				{
					if (key.ToLower().StartsWith("dbconn"))
					{
						list.Add(key);
					}
				}
				return list;
			}

			public static Dictionary<string, string> GetDBConnMap()
			{
				Dictionary<string, string> dictionary = new Dictionary<string, string>();
				foreach (string key in ConfigurationSettings.AppSettings.Keys)
				{
					if (key.ToLower().StartsWith("dbconn"))
					{
						dictionary[key] = ConfigurationSettings.AppSettings[key];
					}
				}
				return dictionary;
			}

			public static IDataReader GetRS(string Sql, SqlConnection dbconn)
			{
			   SqlCommand sqlCommand = new SqlCommand(Sql, dbconn);
				return sqlCommand.ExecuteReader();
			}

			public static string SQuote(string s)
			{
				int capacity = s.Length + 25;
				StringBuilder stringBuilder = new StringBuilder(capacity);
				stringBuilder.Append("N'");
				stringBuilder.Append(s.Replace("'", "''"));
				stringBuilder.Append("'");
				return stringBuilder.ToString();
			}

			public static string RSField(IDataReader rs, string fieldname)
			{
				int ordinal = rs.GetOrdinal(fieldname);
				if (rs.IsDBNull(ordinal))
				{
					return string.Empty;
				}
				return rs.GetString(ordinal);
			}

			public static int RSFieldInt(IDataReader rs, string fieldname)
			{
				int ordinal = rs.GetOrdinal(fieldname);
				if (rs.IsDBNull(ordinal))
				{
					return 0;
				}
				return rs.GetInt32(ordinal);
			}

			public static bool RSFieldBool(IDataReader rs, string fieldname)
			{
				int ordinal = rs.GetOrdinal(fieldname);
				if (rs.IsDBNull(ordinal))
				{
					return false;
				}
				string text = rs[fieldname].ToString();
				return text.Equals("TRUE", StringComparison.InvariantCultureIgnoreCase) || text.Equals("YES", StringComparison.InvariantCultureIgnoreCase) || text.Equals("1", StringComparison.InvariantCultureIgnoreCase);
			}

			public static void ExecuteSQL(string Sql, string connString)
			{
				SqlConnection sqlConnection = new SqlConnection();
				sqlConnection.ConnectionString = connString;
				sqlConnection.Open();
				SqlCommand sqlCommand = new SqlCommand(Sql, sqlConnection);
				try
				{
					sqlCommand.ExecuteNonQuery();
					sqlCommand.Dispose();
					sqlConnection.Close();
					sqlConnection.Dispose();
				}
				catch (Exception ex)
				{
					sqlCommand.Dispose();
					sqlConnection.Close();
					sqlConnection.Dispose();
					throw ex;
				}
			}
		}

		public static class AppLogic
		{
			public static readonly string ro_TBD = "TBD";

			public static Dictionary<string, string> AppConfigCache = new Dictionary<string, string>();

			public static string SafeStr(object stringValue)
			{
				return (stringValue == null) ? string.Empty : stringValue.ToString().Trim();
			}

			public static string AppConfig(string name, string connString)
			{
				if (AppConfigCache.ContainsKey(name.ToLower()))
				{
					return AppConfigCache[name.ToLower()];
				}
				return ConfigurationSettings.AppSettings[name];
			}

			public static void SendMail(int SendMailRetryId, string subject, string body, bool useHTML, string fromaddress, string fromname, string toaddresslist, string toname, string bccaddresses, string ReplyTo, string server, string attachment, string connString)
			{
				string[] array = toaddresslist.Split(";".ToCharArray());
				foreach (string address in array)
				{
					if (server.Equals(ro_TBD, StringComparison.InvariantCultureIgnoreCase) || server.Equals("MAIL.YOURDOMAIN.COM", StringComparison.InvariantCultureIgnoreCase) || server.Length == 0)
					{
						continue;
					}
					MailMessage mailMessage = null;
					try
					{
						mailMessage = new MailMessage(new MailAddress(fromaddress, fromname), new MailAddress(address, toname));
						if (ReplyTo.Length != 0)
						{
							mailMessage.ReplyTo = new MailAddress(ReplyTo);
						}
						mailMessage.Subject = subject;
						mailMessage.Body = body;
						mailMessage.IsBodyHtml = useHTML;
                        //if (!string.IsNullOrEmpty(attachment) && attachment.Trim().Length != 0)
                        //{
                        //    mailMessage.Attachments.Add(new Attachment(attachment));
                        //}
                        if (bccaddresses.Length != 0)
						{
							MailAddressCollection mailAddressCollection = new MailAddressCollection();
							string[] array2 = bccaddresses.Split(',', ';');
							foreach (string text in array2)
							{
								if (text.Trim().Length > 0)
								{
									mailMessage.Bcc.Add(new MailAddress(text.Trim()));
								}
							}
						}
						SmtpClient smtpClient = new SmtpClient(server);
						if (AppConfig("MailMe_User", connString).Length != 0)
						{
							NetworkCredential credentials = new NetworkCredential(AppConfig("MailMe_User", connString), AppConfig("MailMe_Pwd", connString));
							smtpClient.UseDefaultCredentials = false;
							smtpClient.Credentials = credentials;
						}
						else
						{
							smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
						}
						smtpClient.EnableSsl = AppConfigBool("MailMe_UseSSL", connString);
						smtpClient.Port = Convert.ToInt32(AppConfig("MailMe_Port", connString));
						smtpClient.Send(mailMessage);
						DB.ExecuteSQL("UPDATE SendMailRetry SET Success=1 WHERE SendMailRetryId=" + SendMailRetryId, connString);
					}
					catch (Exception ex)
					{
						DB.ExecuteSQL("UPDATE SendMailRetry SET ExceptionMessages=ExceptionMessages+'<hr />'+" + DB.SQuote(ex.ToString()) + " WHERE SendMailRetryId=" + SendMailRetryId, connString);
					}
					finally
					{
						mailMessage.Dispose();
						DB.ExecuteSQL("UPDATE SendMailRetry SET Retries=Retries+1, LastResentOn=getdate()  WHERE SendMailRetryId=" + SendMailRetryId, connString);
					}
				}
			}

			public static bool AppConfigBool(string paramName, string connString)
			{
				string text = AppConfig(paramName, connString);
				if (text.Equals("TRUE", StringComparison.InvariantCultureIgnoreCase) || text.Equals("YES", StringComparison.InvariantCultureIgnoreCase) || text.Equals("1", StringComparison.InvariantCultureIgnoreCase))
				{
					return true;
				}
				return false;
			}

			public static int AppConfigNativeInt(string paramName, string connString)
			{
				string s = AppConfig(paramName, connString);
				return Localization.ParseNativeInt(s);
			}
		}

		public static class Localization
		{
			public static int ParseNativeInt(string s)
			{
				int.TryParse(s, NumberStyles.Integer, Thread.CurrentThread.CurrentUICulture, out var result);
				return result;
			}
		}

		private static void Main(string[] args)
		{
			foreach (string dBConn in DB.GetDBConnList())
			{
				string text = DB.GetDBConnMap()[dBConn];
				try
				{
					 SqlConnection sqlConnection = new SqlConnection(text);
					sqlConnection.Open();
					 IDataReader dataReader = DB.GetRS("select * from SendMailRetry with(nolock) where Success=0 and Retries<2  order by SendMailRetryId", sqlConnection);
					while (dataReader.Read())
					{
						int sendMailRetryId = DB.RSFieldInt(dataReader, "SendMailRetryId");
						string subject = DB.RSField(dataReader, "Subject");
						string body = DB.RSField(dataReader, "Body");
						string attachment = DB.RSField(dataReader, "Attachment");
						string fromaddress = DB.RSField(dataReader, "FromAddress");
						string fromname = DB.RSField(dataReader, "FromName");
						bool useHTML = DB.RSFieldBool(dataReader, "UseHtml");
						string toaddresslist = DB.RSField(dataReader, "ToAddress");
						string toname = DB.RSField(dataReader, "ToName");
						string bccaddresses = DB.RSField(dataReader, "BccAddress");
						string replyTo = DB.RSField(dataReader, "ReplyTo");
						string text2 = ConfigurationSettings.AppSettings["SmtpServerOverride"];
						if (string.IsNullOrEmpty(text2))
						{
							text2 = DB.RSField(dataReader, "MailServer");
						}
						AppLogic.SendMail(sendMailRetryId, subject, body, useHTML, fromaddress, fromname, toaddresslist, toname, bccaddresses, replyTo, text2, attachment, text);
					}
				}
				catch (Exception ex)
				{
					SysLog.LogException(ex, dBConn);
				}
			}
		}
	}
	}
